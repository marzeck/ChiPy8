# ChiPy8 is an chip8 emulator written in python.
# How To Run:
# 0. run command: python3 ChiPy8.py resolution gamespeed game screenColor
# 1. resolution input in '1280x640' format. Use only 2:1 aspect ratio!
# 2. gamespeed - sets gamespeed default is 0.001 (The lower value is the faster emulator runs) 0.001 is good value for most roms but some eg. tetris needs slower speed like 0.003
# 3. game - here input name of the game 
# 4. screenColor - default color is white but you can use many colors eg.(red, blue, grey, yellow)
# Project runs properly on Linux